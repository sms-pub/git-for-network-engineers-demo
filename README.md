# git-for-network-engineers-demo

This repo creates and configures one or more Cisco 8000v routers on AWS.  This repo is for demonstration purposes only an has examples for automating Terraform deployments using GitLab CI/CD.

[Diagrams on draw.io](https://app.diagrams.net/?libs=general;aws4&src=about#Asms-pub%2Fgit-for-network-engineers-terraform-demo%2Fmain%2Fdiagrams%2Fdefault.drawio)

### Reference
[Cisco SD-WAN Cloud onRamp for Multi-Cloud lab](https://github.com/CiscoDevNet/sdwan-cor-labinfra)

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)   
- [Inputs](#inputs)
- [Outputs](#outputs)
- [Contributing](#contributing)

## Usage
Use this as a template for Terraform projects integrated with Terraform Cloud/Enterprise and CI/CD.  This template includes
examples of how to test and validate Terraform with CI/CD.

### Pre-requisites
If using multiple Terraform Workspaces with this template you must set the following environment variable in GitLab.

| Key | Value |
|------|------|
| MULTI_WORKSPACE | true |

### Quick Start
```bash
terraform login
terraform init
terraform validate
terraform plan
terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ec2-instance"></a> [ec2-instance](#module\_ec2-instance) | terraform-aws-modules/ec2-instance/aws | 3.4.0 |

## Resources

| Name | Type |
|------|------|
| [aws_security_group.allow_ssh](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/resources/security_group) | resource |
| [aws_ami.ami](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/data-sources/ami) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/data-sources/caller_identity) | data source |
| [aws_iam_account_alias.current](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/data-sources/iam_account_alias) | data source |
| [aws_subnet.subnets](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/data-sources/subnet) | data source |
| [aws_subnets.subnets](https://registry.terraform.io/providers/hashicorp/aws/4.0.0/docs/data-sources/subnets) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_id"></a> [account\_id](#input\_account\_id) | Account ID to deploy in | `string` | n/a | yes |
| <a name="input_allowed_subnets"></a> [allowed\_subnets](#input\_allowed\_subnets) | Subnets allowed to SSH to instances | `list(string)` | <pre>[<br>  "172.31.0.0/16"<br>]</pre> | no |
| <a name="input_ami_account_id"></a> [ami\_account\_id](#input\_ami\_account\_id) | Account ID where the AMIs are created | `string` | n/a | yes |
| <a name="input_ami_search_string"></a> [ami\_search\_string](#input\_ami\_search\_string) | n/a | `string` | `"Cisco-C8K-17.07.01a-42cb6e93-8d9d-490b-a73c-e3e56077ffd1"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region | `string` | `"us-east-1"` | no |
| <a name="input_azs"></a> [azs](#input\_azs) | AZs in us-gov-east | `list(string)` | <pre>[<br>  "us-east-1a",<br>  "us-east-1b",<br>  "us-east-1c"<br>]</pre> | no |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | A map of tags to add to all resources | `map(string)` | <pre>{<br>  "org": "sms",<br>  "team": "tlp",<br>  "tf-owned": "true"<br>}</pre> | no |
| <a name="input_dynamic_tags"></a> [dynamic\_tags](#input\_dynamic\_tags) | A map of dynamic tags updated by CI/CD | `map(string)` | <pre>{<br>  "branch": "branch",<br>  "env": "env",<br>  "repo": "repo"<br>}</pre> | no |
| <a name="input_instances"></a> [instances](#input\_instances) | Defines a list of instances and their names | `list(any)` | <pre>[<br>  "netdev-01"<br>]</pre> | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Key name of the Key Pair to use for the instance | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Default resource name | `string` | `"netdev"` | no |
| <a name="input_public_key_hash"></a> [public\_key\_hash](#input\_public\_key\_hash) | SSH Public Key Hash | `string` | `"EC0907F8069C51745FC515C10A1C7B47"` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID to deploy in | `string` | n/a | yes |
| <a name="input_vpc_subnet_tags"></a> [vpc\_subnet\_tags](#input\_vpc\_subnet\_tags) | Map of tags used to query for subnets | `map(string)` | <pre>{<br>  "Network": "Public"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_ids"></a> [instance\_ids](#output\_instance\_ids) | n/a |
| <a name="output_public_ips"></a> [public\_ips](#output\_public\_ips) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)  
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)
  
#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

NOTE: `pre-commit` requires [Python](https://www.python.org/), `terraform-docs` requires [golang](https://golang.org/doc/install).  

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```
In the output, GitLab prompts you with a direct link for creating a merge request:
```bash
...
remote: To create a merge request for docs-new-merge-request, visit:
remote:   https://gitlab-instance.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
```

# License
Copyright (c) 2020 [SMS Data Products](https://www.sms.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
