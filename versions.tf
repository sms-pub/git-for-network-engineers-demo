terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.0.0"
    }
  }
  required_version = ">= 1.1.5"
  backend "remote" {
    organization = "sms"
    hostname     = "app.terraform.io"
    workspaces {
      // prefix = "git-for-network-engineers-terraform-demo-" // Multi Workspace
      name = "git-for-network-engineers-terraform-demo" // Single Workspace
    }
  }
}